#[derive(Debug)]
enum Colour {
    RGB(u8, u8, u8),
    HSL(u16, f64, f64)
}

fn main() {

    let red = Colour::RGB(255, 0, 0);
    let purple = Colour::HSL(261, 56.0, 51.0); 

    println!("{:?}", red);
    println!("{:?}", purple);
}
