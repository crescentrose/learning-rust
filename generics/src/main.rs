fn largest<T>(list: &[T]) -> T
    where T: PartialOrd + Copy 
{
    let mut largest = list[0];

    for &number in list.iter() {
        if number > largest {
            largest = number;
        }
    }

    largest
}

struct Tweet {
    content: String,
    username: String
}

struct Article {
    headline: String,
    content: String,
    author: String
}

struct Weather {
    condition: String,
    temperature: i32
}

trait Summarizable {
    fn author(&self) -> String;

    fn summary(&self) -> String {
        format!("Read more by {}", self.author())
    }
}

impl Summarizable for Tweet {
    fn author(&self) -> String {
        format!("@{}", self.username)
    }
}

impl Summarizable for Article {
    fn author(&self) -> String {
        self.author.clone()
    }

    fn summary(&self) -> String {
        format!("{}, by {}", self.headline, self.author)
    }
}

impl Summarizable for Weather {
    fn summary(&self) -> String {
        format!("Weather for today is {} with a high of {} C!", self.condition, self.temperature)
    }

    fn author(&self) -> String {
        String::from("Weather report")
    }
}

fn main() {
    let tweet = Tweet {
        content: String::from("kifla"),
        username: String::from("burek"),
    };

    let article = Article {
        content: String::from("some content"),
        headline: String::from("Sample headline"),
        author: String::from("foobar")
    };

    let weather = Weather {
        condition: String::from("Fair"),
        temperature: 21,
    };

    let numbers = vec![50, 30, 99, 11, 64];

    println!("The largest number is {}", largest(&numbers));

    println!("{}", tweet.summary());
    println!("{}", article.summary());
    println!("{}", weather.summary());
}
