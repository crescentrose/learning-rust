use std::fs::File;
use std::io::ErrorKind;
use std::io::Read;

fn main() {
    let f = File::open("hello.txt");

    let mut f = match f {
        Ok(file) => file,
        Err(ref error) if error.kind() == ErrorKind::NotFound => {
            match File::create("hello.txt") {
                Ok(new_file) => new_file,
                Err(new_error) => panic!("Error creating file: {:?}", new_error)
            }
        },
        Err(error) => {
            panic!("Error opening file: {:?}", error);
        }
    };

    let mut data = String::new();
    f.read_to_string(&mut data).expect("Can't read file :(");

    print!("{}", data);
}
