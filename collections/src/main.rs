use std::collections::HashMap;

#[derive(Debug)]
struct Temperature {
    degrees: i32
}

#[derive(Debug)]
struct Precipitation {
    cm_of_water: i32
}

#[derive(Debug)]
enum Weather {
    Sunny(Temperature),
    Cloudy(Temperature),
    Rainy(Temperature, Precipitation),
}

fn main() {

    let mut daily_forecast = vec![
        Weather::Sunny(Temperature { degrees: 25 }),
        Weather::Sunny(Temperature { degrees: 23 }),
        Weather::Rainy(Temperature { degrees: 19 }, Precipitation { cm_of_water: 6 })
    ];

    daily_forecast.push(Weather::Cloudy(Temperature { degrees: 20 }));

    let days = vec![
        "Today", "Tomorrow", "In 2 days", "In 3 days"
    ];

    let daily_forecast: HashMap<_, _> = days.iter().zip(daily_forecast.iter()).collect();
    
    for (day, forecast) in daily_forecast {
        let mut output = String::from("The forecast is: ");
        match forecast {
            &Weather::Sunny(ref temp) => {
                let o = format!("☀️  sunny with a temperature of {} degrees.", temp.degrees);
                output.push_str(&o);
            },
            &Weather::Cloudy(ref temp) => {
                let o = format!("☁️  overcast with a temperature of {} degrees", temp.degrees);
                output.push_str(&o);
            },
            &Weather::Rainy(ref temp, ref prec) => {
                let o = format!("🌧  rainy with a temperature of {} degrees, {} cm of water.", temp.degrees, prec.cm_of_water);
                output.push_str(&o);
            },
        }

        println!("{}: {}", day, output);
    }
}
